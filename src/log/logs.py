# logs.py
#
# Copyright 2020 Gildemberg Santos <gildemberg.santos@gmail.com>
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to
# deal in the Software without restriction, including without limitation the
# rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
# sell copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
# FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
# IN THE SOFTWARE.
#
# SPDX-License-Identifier: MIT


import time
from src.dao.mongodao import MongoDao
from src.telegram.telegram import Telegram
from src.timestamp import Timestamp

class Logs(object):
    
    def __init__(self, type_log, start=True, output_dao=True, output_telegram=True, output_print=True, time_limit=10):
        self.start_time = 0
        self.finally_time = 0
        self.time_now = 0
        self.time_limit = time_limit
        self.type_log = type_log
        self.output_dao = output_dao
        self.output_telegram = output_telegram
        self.output_print = output_print

        if start:
            self.start()
    
    def start(self):
        self.start_time = time.time()

    def stop(self):
        self.finally_time = time.time()
        self.time_now = (self.finally_time - self.start_time)
        msg = 'Tipo {0} | time {1:.2f}'.format(self.type_log, self.time_now)
        # Gravar log no banco
        if self.output_dao:
            try:
                ts = Timestamp()
                dao = MongoDao()
                dao.delete(dao.dados_log, {'type':self.type_log, 'ts':{'$lt':ts.t_lost(lost=3600)}})
                dao.insert(dao.dados_log, {'type':self.type_log, 'time': self.time_now, 'ts': ts.t_now()})
                msg = 'Tipo {0} | time {1:.2f} | media {2:.2f}'.format(self.type_log, self.time_now, self.media(self.type_log))
                # if (self.time_now > self.time_limit) and self.type_log == 'tempo_total':
                #     dao.delete(dao.dados_log, {'type':'time_over', 'ts':{'$lt':ts.t_lost(lost=3600)}})
                #     dao.insert(dao.dados_log, {'type':'time_over', 'time': (self.time_now - self.time_limit), 'ts':ts.t_now()})
                #     msg = 'Tipo {0} | time {1:.2f} | media {3:.2f} | over {2:.2f} | media {4:.2f}'.format(self.type_log, self.time_now, (self.time_now - self.time_limit), self.media(self.type_log), self.media('time_over'))
            except Exception as e:
                print('Erro: ', e.__context__)
        # Enviar log pelo telegram
        if self.output_telegram:
            try:
                tel = Telegram()
                tel.enviar(msg)
            except Exception as e:
                print('Erro: ', e.__context__)
        # Imprimir
        if self.output_print:
            print(msg)
    
    def gravar(self, type_log, context):
        dao = MongoDao()
        # dao.delete(dao.dados_log, {'type':type_log})
        dao.insert(dao.dados_log, {'type':type_log, 'context': context})

    def media(self, type_log):
        dao = MongoDao()
        lista = dao.find(tb=dao.dados_log, filter={'type': type_log})
        count = 1
        tempo = 1
        for item in lista:
            count += 1
            tempo += item['time']
        media = float(tempo / count)
        if media > 15:
            dao.delete(tb=dao.dados_log, filter={'type': type_log, 'time':{'$gte': 10}})
        return float(tempo / count)
        
