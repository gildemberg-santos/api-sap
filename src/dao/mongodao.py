# mongodao.py
#
# Copyright 2020 Gildemberg Santos <gildemberg.santos@gmail.com>
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to
# deal in the Software without restriction, including without limitation the
# rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
# sell copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
# FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
# IN THE SOFTWARE.
#
# SPDX-License-Identifier: MIT


from pymongo import MongoClient


class Connection(object):
    def __init__(self, host=None, port=None):
        self.host = host
        self.port = port


class MongoDao(object):
    def __init__(self, conn=Connection('localhost', 27017)):
        cliente = MongoClient(host=conn.host, port=conn.port)
        self.banco = cliente.bd_apostas
        self.dados_log = self.banco.Logs
        self.dados_sport = self.banco.Esporte
        self.dados_categories = self.banco.Torneio
        self.dados_matches = self.banco.Partida
        self.dados_ht = self.banco.Ht
        self.dados_localteam = self.banco.Timelocal
        self.dados_visitorteam = self.banco.Timevisitante
        self.dados_time = self.banco.Times
        self.dados_odds = self.banco.Mercado
        self.dados_bookmakers = self.banco.Casa
        self.dados_odd = self.banco.Odd
        self.dados_tipes = self.banco.Tipes
        self.dados_handicap = self.banco.Handicap
        #
        self.op_tipes = self.banco.Op_Tipes

    def insert(self, tb, d_json):
        tb.insert_one(d_json)

    def update(self, tb, d_json, filter=None):
        tb.update_one(filter=filter, update={"$set": d_json}, upsert=False)

    def upsert(self, tb, d_json, filter=None):
        tb.update_many(filter=filter, update={"$set": d_json}, upsert=True)

    def delete(self, tb, filter=None):
        tb.delete_many(filter=filter)

    def find_one(self, tb, filter=None):
        d_json = tb.find_one(filter=filter)
        return d_json
    
    def find(self, tb, filter=None):
        d_json = tb.find(filter=filter)
        return d_json

    def find_count(self, tb, filter=None):
        d_json = tb.find(filter=filter).count()
        return d_json
    
    def find_distinct(self, tb, distinct=None):
        d_json = tb.distinct(distinct)
        return d_json
    
    

