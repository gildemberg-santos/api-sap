# estrategia.py
#
# Copyright 2020 Gildemberg Santos <gildemberg.santos@gmail.com>
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to
# deal in the Software without restriction, including without limitation the
# rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
# sell copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
# FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
# IN THE SOFTWARE.
#
# SPDX-License-Identifier: MIT


class Estrategia(object):
    def __init__(self, timeA=None, timeB=None, montante=None):
        self.timeA = timeA
        self.timeB = timeB
        self.montante = montante

    def surebetVP(self, timeA=None, timeB=None, montante=100):
        self.timeA = timeA
        self.timeB = timeB
        self.montante = montante

        timeSoma = timeA + timeB

        timeApos = (timeB / timeSoma) * montante
        timeBpos = (timeA / timeSoma) * montante

        timeAstake = (timeA * timeApos) - montante
        timeBstake = (timeB * timeBpos) - montante

        lucropos = (((timeAstake + timeBstake) / 2) / montante) * 100

        print('Soma ODDs %.2f' % timeSoma)
        print('Time A porcentagem %.2f' % timeApos, '%')
        print('Time B porcentagem %.2f' % timeBpos, '%')
        print('Time A stake %.4f' % timeAstake)
        print('Time B stake %.4f' % timeBstake)
        print('Lucro %.2f' % lucropos, '%')

        return lucropos

    def get_porc_timeA(self):
        return (self.timeB / (self.timeA + self.timeB)) * self.montante

if __name__ == "__main__":

    lista_casa = []
    
    lista_casa.append({"corretora":"bet365", "odds":{"timeA":1.5, "timeB":3.10, "empate":4.5}})
    lista_casa.append({"corretora":"sportingbet", "odds":{"timeA":2.05, "timeB":4.0, "empate":6}})
 
    # lista_casa.append({"corretora":"c1", "odds":{"timeA":2.10, "timeB":4, "empate":4.5}})
    # lista_casa.append({"corretora":"c2", "odds":{"timeA":1.5, "timeB":4.30, "empate":6}})
    
    # lista_casa.append({"corretora":"c1", "odds":{"timeA":1.9, "timeB":3.2, "empate":4.5}})
    # lista_casa.append({"corretora":"c2", "odds":{"timeA":1.75, "timeB":3.40, "empate":6}})
    # lista_casa.append({"corretora":"c3", "odds":{"timeA":1.50, "timeB":3.22, "empate":5.5}})
    # lista_casa.append({"corretora":"c4", "odds":{"timeA":1.80, "timeB":3.90, "empate":2}})

    est = Estrategia()
    # est.analize(lista_casa=lista_casa)
    est.surebetVP(1.5, 4, 150)
