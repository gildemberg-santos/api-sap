# scannerjson.py
#
# Copyright 2020 Gildemberg Santos <gildemberg.santos@gmail.com>
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to
# deal in the Software without restriction, including without limitation the
# rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
# sell copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
# FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
# IN THE SOFTWARE.
#
# SPDX-License-Identifier: MIT


import os
import json
import requests
import xmltodict
from src.timestamp import Timestamp
from src.url.addressodds import AddressOdds
from src.entidade.scores import Scores
from src.log.logs import Logs


class ScannerJSON(object):
    def __init__(self):
        pass

    def download(self, url=''):
        log = Logs('download', output_telegram=False, output_print=False)
        ts = Timestamp()
        tempo = ts.t_now()
        try:
            headers = {'accept-encoding': 'gzip'}
            response = requests.get(url, headers=headers)
            if response.status_code == 200:
                data = response.content
                log.stop()
                return {'dado': data, 'ts': tempo}
        except Exception as e:
            print('Erro: ', e.__context__)
            log.stop()
            return {'dado': None, 'ts': tempo}
        

    async def get_odd(self, url=None, time_limit=5):
        log = Logs('get_odd', output_telegram=False, output_print=False, time_limit=time_limit)
        if not url:
            urlodds = AddressOdds()
            url = urlodds.soccer
            del(urlodds)
        scores = Scores()
        obj = scores.select()
        ts = Timestamp()

        try:
            timestamp = ts.t_now(timestamp=obj['ts'])
        except:
            timestamp = ts.t_now()
        ts_temp = ts.t_lost(lost=time_limit)
        if timestamp < ts.t_lost(lost=(time_limit * 10)):
            timestamp = ts_temp
        dados = self.download(url=url % timestamp)
        # self.writejson(dados=dados)
        # dados = self.readjson()
        _json = json.loads(dados['dado'])      
        # await asyncio.wait([scores.salvar(dados=_json)])
        scores.salvar(dados=_json, ts_download=timestamp)
        # scores.salvar2(dados=_json, ts_download=timestamp)
        scores.limpar()
        del(timestamp)
        del(dados)
        del(_json)
        del(obj)
        del(scores)
        log.stop()

    def surebet(self):
        scores = Scores()
        dados = self.readjson()
        _dict = json.loads(dados)
        obj = scores.get_object(dados_json=_dict)
        obj.get_categories(id='')
        # obj = scores.c_dict_to_object(_dict=_dict)
        
    def writejson(self, dados=''):
        try:
            os.mkdir('temp')
        except:
            pass

        try:
            with open("{0}{1}.{2}".format('temp/', 'soccer', 'json'), 'w') as fdw:
                json.dump(json.loads(dados), fdw, indent=4)
        except:
            pass
    
    def readjson(self):
        dados = None
        try:
            with open("{0}{1}.{2}".format('temp/', 'soccer', 'json'), 'r') as fdr:
                dados = fdr.read()
        except:
            pass
        return dados

    def convert_xml_to_json(self, xml):
        doc = xmltodict.parse(xml)
        json.dumps(doc)
