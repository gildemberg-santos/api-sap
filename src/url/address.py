# address.py
#
# Copyright 2020 Gildemberg Santos <gildemberg.santos@gmail.com>
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to
# deal in the Software without restriction, including without limitation the
# rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
# sell copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
# FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
# IN THE SOFTWARE.
#
# SPDX-License-Identifier: MIT


import requests
import json
import os
from src.url.addresscsv import AddressCSV


class Address(object):
    def __init__(self):
        self.server = ""
        self.odds = ""
        self.mongodb = ""
        self.portadb = 0

        try:
            os.mkdir('data')
        except:
            pass
        address = []
        address = AddressCSV().get_address('data/address.csv')

        for item in address:
            if item[0] == 'server':
                self.server = item[1]
            if item[0] == 'odds':
                self.odds = item[1]
            if item[0] == 'mongodb':
                self.mongodb = item[1]
            if item[0] == 'portadb':
                try:
                    self.portadb = int(item[1])
                except:
                    self.portadb = 0
                

    def get_address_server_json(self):
        return self.get_json(url=self.server)

    def get_address_odds_json(self):
        return self.get_json(url=self.odds)

    def get_json(self, url):
        try:
            headers = {'accept-encoding': 'gzip'}
            r = requests.get(url, headers=headers)
            if r.status_code == 200:
                valor = r.content.decode(
                    'UTF-8').replace('\n', '').replace('  ', '')
                dados_json = json.loads(valor)
                return dados_json
            else:
                return None
        except Exception as e:
            print('Erro: ', e.__context__)
            return None
        
