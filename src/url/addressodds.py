# addressodds.py
#
# Copyright 2020 Gildemberg Santos <gildemberg.santos@gmail.com>
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to
# deal in the Software without restriction, including without limitation the
# rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
# sell copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
# FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
# IN THE SOFTWARE.
#
# SPDX-License-Identifier: MIT


from src.url.address import Address
from src.url.addressserver import AddressServer


class AddressOdds(object):
    def __init__(self, dados_json=None):
        if not dados_json:
            address = Address()
            dados_json = address.get_address_odds_json()
        self.dados_json = dados_json
        server = AddressServer()
        url = server.url
        self.soccer = "{0}{1}".format(url, self.get_odds(sport='soccer'))
        self.basket = "{0}{1}".format(url, self.get_odds(sport='basket'))
        self.tennis = "{0}{1}".format(url, self.get_odds(sport='tennis'))
        self.hockey = "{0}{1}".format(url, self.get_odds(sport='hockey'))
        self.handball = "{0}{1}".format(url, self.get_odds(sport='handball'))
        self.volleyball = "{0}{1}".format(
            url, self.get_odds(sport='volleyball'))
        self.football = "{0}{1}".format(url, self.get_odds(sport='football'))
        self.baseball = "{0}{1}".format(url, self.get_odds(sport='baseball'))
        self.cricket = "{0}{1}".format(url, self.get_odds(sport='cricket'))
        self.rugby = "{0}{1}".format(url, self.get_odds(sport='rugby'))
        self.rugbyleague = "{0}{1}".format(
            url, self.get_odds(sport='rugbyleague'))
        self.boxing = "{0}{1}".format(url, self.get_odds(sport='boxing'))
        self.esports = "{0}{1}".format(url, self.get_odds(sport='esports'))
        self.futsal = "{0}{1}".format(url, self.get_odds(sport='futsal'))
        self.mma = "{0}{1}".format(url, self.get_odds(sport='mma'))
        self.table_tennis = "{0}{1}".format(
            url, self.get_odds(sport='table_tennis'))

    def get_odds(self, sport=''):
        if self.dados_json != None:
            for item in self.dados_json:
                if item == sport:
                    return self.dados_json[item]
        return ''
