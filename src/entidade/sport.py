# sport.py
#
# Copyright 2020 Gildemberg Santos <gildemberg.santos@gmail.com>
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to
# deal in the Software without restriction, including without limitation the
# rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
# sell copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
# FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
# IN THE SOFTWARE.
#
# SPDX-License-Identifier: MIT


from src.entidade.categories import Categories


class Sport(object):
    def __init__(self, name=None, ts=None, categories=[Categories()]):
        self.name = name
        self.ts = ts
        self.categories = categories

    def imprimir(self):
        print('name: ', self.name)
        print('ts: ', self.ts)

    def get_list_id(self):
        id = []
        for item in self.categories:
            id.append(item.id)
        return id

    def existing_categories(self, categories=Categories()):
        self.now_categories = []
        for item in self.categories:
            if categories.id == item.id:
                return True
        # self.categories.append(categories)
        return False

    def update_obj(self, obj):
        for item_categories in obj.categories:
            if not self.existing_categories(item_categories):
                self.categories.append(item_categories)

    def get_categories(self, name=None, gid=None, id=None):
        lista_categories = []
        for item in self.categories:
            if name or gid or id:
                if name == item.name and name:
                    lista_categories.append(item)
                elif gid == item.gid and gid:
                    lista_categories.append(item)
                elif id == item.id and id:
                    lista_categories.append(item)
            else:
                lista_categories.append(item)
        if len(lista_categories) <= 0:
            print('Nenhum item foi encontrado!')
        else:
            print('%s itens encontrados' % len(lista_categories))
        return lista_categories
