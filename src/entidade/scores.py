# scores.py
#
# Copyright 2020 Gildemberg Santos <gildemberg.santos@gmail.com>
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to
# deal in the Software without restriction, including without limitation the
# rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
# sell copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
# FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
# IN THE SOFTWARE.
#
# SPDX-License-Identifier: MIT


from src.entidade.sport import Sport
from src.entidade.categories import Categories
from src.entidade.matches import Matches
from src.entidade.localteam import Localteam
from src.entidade.visitorteam import Visitorteam
from src.entidade.ht import Ht
from src.entidade.odds import Odds
from src.entidade.bookmakers import Bookmakers
from src.entidade.tipe import Tipe
from src.entidade.odd import Odd
from src.dao.mongodao import MongoDao
from src.timestamp import Timestamp
from src.log.logs import Logs
import json
from src.telegram.telegram import Telegram

class Scores(object):
    def __init__(self):
        pass

    def salvar(self, dados, ts_download):
        log = Logs('salvar', output_telegram=False, output_print=False)
        obj = self.get_object(dados_json=dados)
        # Esporte
        temp_esporte = self.c_object_to_dict(obj=obj)
        self.salvar_esporte(temp_esporte, ts_download)
        # Torneio
        temp_categories = self.c_object_to_dict(obj=obj)
        for item_categories in temp_categories['categories']:
            # Partida   
            for item_matches in item_categories['matches']:
                validade = self.validar(item_matches)
                if not validade:
                    break
                # Odds
                for item_odds in item_matches['odds']:
                    # Bookmakers
                    for item_bookmakers in item_odds['bookmakers']:  
                        # Odd
                        # for item_odd in item_bookmakers['odd']:
                        #     pass
                        # Tipe
                        for item_tipe in item_bookmakers['tipe']:
                            pass
                            # Odd
                            for item_tipe_odd in item_tipe['odd']:
                                self.salvar_handicap(temp_esporte, item_categories, item_matches, item_odds, item_bookmakers, item_tipe, item_tipe_odd, ts_download)
                            # Tipe
                        # Casa
                        self.salvar_casa(item_bookmakers, ts_download)
                    # Mercado
                    self.salvar_mercado(item_odds, ts_download)
                # Partida
                self.salvar_partida(item_matches, ts_download)
            # Torneio
            self.salvar_torneiro(item_categories, ts_download)
        log.stop()

    def salvar_esporte(self, esporte, ts_download):
        dao = MongoDao()
        esporte['nome'] = esporte['name']
        del esporte['name']
        del esporte['categories']
        esporte['ts'] = ts_download
        dao.upsert(tb=dao.dados_sport, d_json=esporte, filter={"nome": esporte["nome"]})

    def salvar_torneiro(self, torneiro, ts_download):
        dao = MongoDao()
        torneiro['id'] = int(torneiro['id'])
        del torneiro['gid']
        del torneiro['matches']
        torneiro['nome'] = torneiro['name']
        del torneiro['name']
        torneiro['ts'] = ts_download
        dao.upsert(tb=dao.dados_categories, d_json=torneiro, filter={"id": int(torneiro['id'])})

    def validar(self, partida):
        ts = Timestamp()
        partida['data'] = ts.f_data_str(ts.f_ts(partida['formatted_date'], partida['time']))
        partida['hora'] = ts.f_hora_str(ts.f_ts(partida['formatted_date'], partida['time']))
        partida['ts_partida'] = ts.f_ts(partida['formatted_date'], partida['time'])
        # print(partida['ts_partida'], ts.t_now())
        if partida['ts_partida'] > ts.t_now():
            return True
        else:
            return False

    def salvar_partida(self, partida, ts_download):
        ts = Timestamp()
        dao = MongoDao()
        partida['id'] = int(partida['id'])
        del partida['odds']
        del partida['ht']
        partida['id_timelocal'] = int(partida['localteam']['id'])
        partida['localteam']['id'] = int(partida['localteam']['id'])
        partida['localteam']['nome'] = partida['localteam']['name']
        partida['localteam']['ts'] = ts_download
        del partida['localteam']['name']
        del partida['localteam']['goals']
        dao.upsert(tb=dao.dados_time, d_json=partida['localteam'], filter={"id": int(partida['localteam']['id'])})
        del partida['localteam']
        partida['id_timevisitante'] = int(partida['visitorteam']['id'])
        partida['visitorteam']['id'] = int(partida['visitorteam']['id'])
        partida['visitorteam']['nome'] = partida['visitorteam']['name']
        partida['visitorteam']['ts'] = ts_download
        del partida['visitorteam']['name']
        del partida['visitorteam']['goals']
        dao.upsert(tb=dao.dados_time, d_json=partida['visitorteam'], filter={"id": int(partida['visitorteam']['id'])})
        del partida['visitorteam']
        del partida['date']
        del partida['events']
        del partida['fix_id']
        del partida['static_id']
        del partida['status']
        del partida['venue']
        partida['data'] = ts.f_data_str(ts.f_ts(partida['formatted_date'], partida['time']))
        partida['hora'] = ts.f_hora_str(ts.f_ts(partida['formatted_date'], partida['time']))
        partida['ts_partida'] = ts.f_ts(partida['formatted_date'], partida['time'])
        partida['ts'] = ts_download
        del partida['formatted_date']
        del partida['time']
        dao.upsert(tb=dao.dados_matches, d_json=partida, filter={"id": int(partida['id'])})

    def salvar_mercado(self, mercado, ts_download):
        dao = MongoDao()
        mercado['id'] = int(mercado['id'])
        mercado['nome'] = mercado['value']
        mercado['ts'] = ts_download
        del mercado['bookmakers']
        del mercado['stop']
        del mercado['value']
        dao.upsert(tb=dao.dados_odds, d_json=mercado, filter={"id": int(mercado['id'])})
        
    def salvar_casa(self, casa, ts_download):
        dao = MongoDao()
        casa['id'] = int(casa['id'])
        casa['nome'] = casa['name']
        casa['url'] = ""
        casa['ts'] = ts_download
        del casa['odd']
        del casa['tipe']
        del casa['stop']
        del casa['name']
        count = dao.find_count(tb=dao.dados_bookmakers, filter={"id": int(casa['id'])})
        if count == 0:
            dao.upsert(tb=dao.dados_bookmakers, d_json=casa, filter={"id": int(casa['id'])})

    def salvar_handicap(self, esporte, torneio, partida, mercado, casa, handicap, odd, ts_download):
        hc = {}
        # idtorneio = [1148, 1141, 2076, 4228, 1129, 1145, 1127, 1139, 1133, 1124, 1140, 1137, 1142, 1144, 1135]
        # if not(int(torneio['id']) in idtorneio):
        #     return
        idmercado = [4, 79, 22601, 22600]
        if not(int(mercado['id']) in idmercado):
            return
        idcasa = [21, 56, 16, 105, 231]
        if not(int(casa['id']) in idcasa):
            return
        dao = MongoDao()
        hc['esporte'] = esporte['nome']
        hc['id_torneio'] = int(torneio['id'])
        hc['id_partida'] = int(partida['id'])
        hc['id_mercado'] = int(mercado['id'])
        hc['id_casa'] = int(casa['id'])
        hc['id_timelocal'] = int(partida['localteam']['id'])
        hc['id_timevisitante'] = int(partida['visitorteam']['id'])
        hc['nome'] = handicap['name']
        hc['valor'] = float(handicap['name'])
        hc['ismain'] = bool(handicap['ismain'])
        hc['stop'] = bool(handicap['stop'])
        hc['mercado_stop'] = bool(mercado['stop'])
        hc['casa_stop'] = bool(casa['stop'])
        hc['tipo'] = handicap['tipo']
        hc['odd_nome'] = odd['name']
        hc['odd_valor'] = float(odd['value'])
        hc['ts'] = ts_download
        # filter = {"esporte":hc['esporte'], "id_torneio":hc['id_torneio'], "id_partida":hc['id_partida'], "id_mercado":hc['id_mercado'], "id_casa":hc['id_casa'], "nome":hc['nome'], "valor":hc['valor'], "ismain":hc['ismain'], "stop":hc['stop'], "mercado_stop":hc['mercado_stop'], "casa_stop":hc['casa_stop'], "tipo":hc['tipo'], "odd_nome":hc['odd_nome'], "odd_valor":hc['odd_valor']}
        filter = {"esporte":hc['esporte'], "id_torneio":hc['id_torneio'], "id_partida":hc['id_partida'], "id_mercado":hc['id_mercado'], "id_casa":hc['id_casa'], "nome":hc['nome'], "valor":hc['valor'], "tipo":hc['tipo'], "odd_nome":hc['odd_nome'], "odd_valor":hc['odd_valor']}
        # filter = {"esporte":hc['esporte'], "id_torneio":hc['id_torneio'], "id_partida":hc['id_partida'], "id_mercado":hc['id_mercado'], "id_casa":hc['id_casa'], "tipo":hc['tipo'], "odd_nome":hc['odd_nome']}
        dao.upsert(tb=dao.dados_handicap, d_json=hc, filter=filter)

    def salvar2(self, dados, ts_download):
        log = Logs('salvar', output_telegram=False, output_print=False)
        ts = Timestamp()
        dao = MongoDao()
        obj = self.get_object(dados_json=dados)

        # Esporte
        temp_esporte = self.c_object_to_dict(obj=obj)
        del temp_esporte['categories']
        temp_esporte['ts'] = int(temp_esporte['ts'])
        dao.upsert(tb=dao.dados_sport, d_json=temp_esporte, filter={"name": temp_esporte["name"]})
        # del temp_esporte

        # Torneio
        temp_categories = self.c_object_to_dict(obj=obj)
        for item_categories in temp_categories['categories']:
            # listacategories = [1141, 1148, 1142, 1144, 1137, 1133, 1124, 1140, 1129, 1007, 1361]
            # if not(item_categories['id'] in listacategories):
            #     print(item_categories['id'])
            #     break

            # Partida   
            for item_matches in item_categories['matches']:

                # Partida
                item_temp_matches = item_matches
                item_temp_matches['ts_matches'] = ts.f_ts(data=item_temp_matches['formatted_date'], time=item_temp_matches['time'])
                if not int(item_temp_matches['ts_matches']) > ts.t_now():
                    break

                # HT
                item_temp_matches_ht = item_temp_matches['ht']
                item_temp_matches_ht['id_matches'] = int(item_matches['id'])
                item_temp_matches_ht['ts'] = ts_download
                count = dao.find_count(tb=dao.dados_ht, filter={"score": item_temp_matches_ht['score'], "id_matches": int(item_temp_matches_ht['id_matches']), "ts": {"$lt": ts_download}})
                if count == 0:
                    dao.upsert(tb=dao.dados_ht, d_json=item_temp_matches_ht, filter={"score": item_temp_matches_ht['score'], "id_matches": int(item_temp_matches_ht['id_matches'])})
                del count

                # Localteam
                item_temp_matches_localteam = item_temp_matches['localteam']
                item_temp_matches_localteam['id_matches'] = int(item_matches['id'])
                item_temp_matches_localteam['id'] = int(item_temp_matches_localteam['id'])
                item_temp_matches_localteam['ts'] = ts_download
                count = dao.find_count(tb=dao.dados_localteam, filter={"name": item_temp_matches_localteam['name'], "id_matches": int(item_temp_matches_localteam['id_matches']), "ts": {"$lt": ts_download}})
                if count == 0:
                    dao.upsert(tb=dao.dados_localteam, d_json=item_temp_matches_localteam, filter={"name": item_temp_matches_localteam['name'], "id_matches": int(item_temp_matches_localteam['id_matches'])})
                del count
                # Visitorteam
                item_temp_matches_visitorteam = item_temp_matches['visitorteam']
                item_temp_matches_visitorteam['id_matches'] = int(item_matches['id'])
                item_temp_matches_visitorteam['id'] = int(item_temp_matches_visitorteam['id'])
                item_temp_matches_visitorteam['ts'] = ts_download
                count = dao.find_count(tb=dao.dados_visitorteam, filter={"name": item_temp_matches_visitorteam['name'], "id_matches": int(item_temp_matches_visitorteam['id_matches']), "ts": {"$lt": ts_download}})
                if count == 0:
                    dao.upsert(tb=dao.dados_visitorteam, d_json=item_temp_matches_visitorteam, filter={"name": item_temp_matches_visitorteam['name'], "id_matches": int(item_temp_matches_visitorteam['id_matches'])})
                del count
                


                # Odds
                for item_odds in item_matches['odds']:
                    

                    # Bookmakers
                    for item_bookmakers in item_odds['bookmakers']:  
                        

                        # Odd
                        for item_odd in item_bookmakers['odd']:
                            item_temp_odd = item_odd
                            item_temp_odd['sport'] = temp_esporte["name"]
                            item_temp_odd['id_categories'] = int(item_categories['id'])
                            item_temp_odd['id_matches'] = int(item_matches['id'])
                            item_temp_odd['id_localteam'] = int(item_temp_matches_localteam['id'])
                            item_temp_odd['id_visitorteam'] = int(item_temp_matches_visitorteam['id'])
                            item_temp_odd['id_odds'] = int(item_odds['id'])
                            item_temp_odd['odds_stop'] = bool(item_odds['stop'])
                            item_temp_odd['id_bookmakers'] = int(item_bookmakers['id'])
                            item_temp_odd['bookmakers_stop'] = bool(item_bookmakers['stop'])
                            item_temp_odd['value'] = float(item_temp_odd['value'])
                            item_temp_odd['ts'] = ts_download
                            # filter_odd = {"odds_stop":item_temp_odd['odds_stop'],"bookmakers_stop":item_temp_odd['bookmakers_stop'],"value":item_temp_odd['value'],"name" : item_temp_odd['name'], "id_matches" : int(item_matches['id']), "id_odds" : int(item_odds['id']), "id_bookmakers": int(item_bookmakers['id']), "ts": {"$lt": ts_download}}
                            # count = dao.find_count(tb=dao.dados_odd, filter=filter_odd)
                            # del filter_odd
                            filter_odd = {"name" : item_temp_odd['name'], "id_matches" : int(item_matches['id']), "id_odds" : int(item_odds['id']), "id_bookmakers": int(item_bookmakers['id'])}
                            # if count == 0:
                            dao.upsert(tb=dao.dados_odd, d_json=item_temp_odd, filter=filter_odd)
                            del filter_odd, item_temp_odd


                        # Tipe
                        for item_tipe in item_bookmakers['tipe']:


                            # Odd
                            for item_tipe_odd in item_tipe['odd']:
                                item_temp_tipe_odd = item_tipe_odd
                                item_temp_tipe_odd['sport'] = temp_esporte["name"]
                                item_temp_tipe_odd['id_categories'] = int(item_categories['id'])
                                item_temp_tipe_odd['id_matches'] = int(item_matches['id'])
                                item_temp_tipe_odd['id_localteam'] = int(item_temp_matches_localteam['id'])
                                item_temp_tipe_odd['id_visitorteam'] = int(item_temp_matches_visitorteam['id'])
                                item_temp_tipe_odd['id_odds'] = int(item_odds['id'])
                                item_temp_tipe_odd['odds_stop'] = bool(item_odds['stop'])
                                item_temp_tipe_odd['id_bookmakers'] = int(item_bookmakers['id'])
                                item_temp_tipe_odd['bookmakers_stop'] = bool(item_bookmakers['stop'])
                                item_temp_tipe_odd['value'] = float(item_temp_tipe_odd['value'])
                                item_temp_tipe_odd['ts'] = ts_download
                                item_temp_tipe_odd['handicap'] = item_tipe['name']
                                item_temp_tipe_odd['handicap_tipo'] = item_tipe['tipo']
                                # filter_tipe_odd = {"handicap_tipo":item_temp_tipe_odd['handicap_tipo'],"handicap":item_temp_tipe_odd['handicap'],"odds_stop":item_temp_tipe_odd['odds_stop'],"bookmakers_stop":item_temp_tipe_odd['bookmakers_stop'],"value" : item_temp_tipe_odd['value'], "name" : item_temp_tipe_odd['name'], "id_matches" : int(item_matches['id']), "id_odds" : int(item_odds['id']), "id_bookmakers": int(item_bookmakers['id']), "ts": {"$lt": ts_download}}
                                # count = dao.find_count(tb=dao.dados_odd, filter=filter_tipe_odd)
                                # del filter_tipe_odd
                                filter_tipe_odd = {"handicap_tipo":item_temp_tipe_odd['handicap_tipo'],"handicap":item_temp_tipe_odd['handicap'],"name" : item_temp_tipe_odd['name'], "id_matches" : int(item_matches['id']), "id_odds" : int(item_odds['id']), "id_bookmakers": int(item_bookmakers['id'])}
                                # if count == 0:
                                dao.upsert(tb=dao.dados_odd, d_json=item_temp_tipe_odd, filter=filter_tipe_odd)
                                del item_temp_tipe_odd, filter_tipe_odd


                            # Tipe
                            item_temp_tipe = item_tipe
                            del item_temp_tipe['odd']
                            item_temp_tipe['sport'] = temp_esporte["name"]
                            item_temp_tipe['id_categories'] = int(item_categories['id'])
                            item_temp_tipe['id_matches'] = int(item_matches['id'])
                            item_temp_tipe['id_localteam'] = int(item_temp_matches_localteam['id'])
                            item_temp_tipe['id_visitorteam'] = int(item_temp_matches_visitorteam['id'])
                            item_temp_tipe['id_odds'] = int(item_odds['id'])
                            item_temp_tipe['odds_stop'] = bool(item_odds['stop'])
                            item_temp_tipe['id_bookmakers'] = int(item_bookmakers['id'])
                            item_temp_tipe['bookmakers_stop'] = bool(item_bookmakers['stop'])
                            item_temp_tipe['ts'] = ts_download
                            item_temp_tipe['ismain'] = bool(item_temp_tipe['ismain'])
                            item_temp_tipe['stop'] = bool(item_temp_tipe['stop'])
                            item_temp_tipe['value'] = float(item_temp_tipe['name'])
                            # filter_tipe = {"stop":item_temp_tipe['stop'],"ismain":item_temp_tipe['ismain'],"name":item_temp_tipe['name'], "bookmakers_stop":item_temp_tipe['bookmakers_stop'], "odds_stop":item_temp_tipe['odds_stop'], "tipo" : item_temp_tipe['tipo'], "id_matches" : int(item_matches['id']), "id_odds" : int(item_odds['id']), "id_bookmakers": int(item_bookmakers['id']), "ts": {"$lt": ts_download}}
                            # count = dao.find_count(tb=dao.dados_tipes, filter=filter_tipe)
                            # del filter_tipe
                            filter_tipe = {"name":item_temp_tipe['name'],"tipo" : item_temp_tipe['tipo'], "id_matches" : int(item_matches['id']), "id_odds" : int(item_odds['id']), "id_bookmakers": int(item_bookmakers['id'])}
                            # if count == 0:
                            dao.upsert(tb=dao.dados_tipes, d_json=item_temp_tipe, filter=filter_tipe)
                            del item_temp_tipe, filter_tipe


                        # Bookmakers
                        item_temp_bookmakers = item_bookmakers
                        del item_temp_bookmakers['odd']
                        del item_temp_bookmakers['tipe']
                        del item_temp_bookmakers['stop']
                        item_bookmakers['id'] = int(item_bookmakers['id'])
                        item_bookmakers['ts'] = ts_download
                        item_bookmakers['url'] = ''
                        filter_bookmakers = {"id" : int(item_temp_bookmakers['id']), "ts": {"$lt": ts_download}}
                        count = dao.find_count(tb=dao.dados_bookmakers, filter=filter_bookmakers)
                        del filter_bookmakers
                        filter_bookmakers = {"id" : int(item_temp_bookmakers['id'])}
                        if count == 0:
                            dao.upsert(tb=dao.dados_bookmakers, d_json=item_temp_bookmakers, filter=filter_bookmakers)
                        del item_temp_bookmakers, filter_bookmakers, count


                    # Odds
                    item_temp_odds = item_odds
                    del item_temp_odds['bookmakers']
                    del item_temp_odds['stop']
                    item_temp_odds['id'] = int(item_temp_odds['id'])
                    item_temp_odds['id_matches'] = int(item_temp_matches['id'])
                    item_temp_odds['ts'] = ts_download
                    # filter_odds = {"id" : int(item_temp_odds['id']), "ts": {"$lt": ts_download}}
                    # count = dao.find_count(tb=dao.dados_odds, filter=filter_odds)
                    # del filter_odds
                    filter_odds = {"id" : int(item_temp_odds['id'])}
                    # if count == 0:
                    dao.upsert(tb=dao.dados_odds, d_json=item_temp_odds, filter=filter_odds)
                    del item_temp_odds, filter_odds

                    
                # Partida
                del item_temp_matches['odds']
                del item_temp_matches['ht']
                del item_temp_matches['localteam']
                del item_temp_matches['visitorteam']
                item_temp_matches['id'] = int(item_temp_matches['id'])
                item_temp_matches['sport'] = temp_esporte["name"]
                item_temp_matches['id_categories'] = int(item_categories['id'])
                item_temp_matches['localteam'] = int(item_temp_matches_localteam['id'])
                item_temp_matches['visitorteam'] = int(item_temp_matches_visitorteam['id'])
                item_temp_matches['ts'] = ts_download
                # count = dao.find_count(tb=dao.dados_matches, filter={"id": int(item_temp_matches['id']), "ts": {"$lt": ts_download}})
                # if count == 0:
                dao.upsert(tb=dao.dados_matches, d_json=item_temp_matches, filter={"id": int(item_temp_matches['id'])})
                del item_temp_matches
            

            # Torneio
            item_temp_categories = item_categories
            del item_temp_categories['matches']
            del item_temp_categories['gid']
            item_temp_categories['sport'] = temp_esporte["name"]
            item_temp_categories['id'] = int(item_temp_categories['id'])
            item_temp_categories['ts'] = ts_download
            # count = dao.find_count(tb=dao.dados_categories, filter={"id": int(item_temp_categories['id']), "ts": {"$lt": ts_download}})                
            # if count == 0:
            dao.upsert(tb=dao.dados_categories, d_json=item_temp_categories, filter={"id": int(item_temp_categories['id'])})
            del item_temp_categories
            
            
        del temp_categories
        log.stop()

    def limpar(self):
        log = Logs('limpar', output_telegram=False, output_print=False)
        ts = Timestamp()
        dao = MongoDao()
        dao.delete(dao.dados_matches, filter={"ts_partida": {"$lt": ts.t_now()}})
        # dao.delete(dao.dados_handicap, filter={"ts": {"$gt": ts.t_lost(lost=600)}})
        # dao.delete(dao.dados_handicap, filter={"id_casa": 21})
        # dao.delete(dao.dados_handicap, filter={"id_casa": 82})
        # dao.delete(dao.dados_handicap, filter={"id_casa": 232})
        # dao.delete(dao.dados_handicap, filter={"id_casa": 88})
        # dao.delete(dao.dados_handicap, filter={"id_casa": 142})
        # dao.delete(dao.dados_handicap, filter={"id_casa": 190})
        # dao.delete(dao.dados_handicap, filter={"id_casa": 230})
        # dao.delete(dao.dados_handicap, filter={"id_casa": 18})
        # dao.delete(dao.dados_handicap, filter={"id_casa": 20})
        # dao.delete(dao.dados_handicap, filter={"id_casa": 2})
        # dao.delete(dao.dados_handicap, filter={"id_casa": 28})
        # dao.delete(dao.dados_handicap, filter={"id_casa": 70})
        # matches = dao.find_one(tb=dao.dados_matches, filter={"ts_partida": {"$lt": ts.t_now()}})
        # try:
        #     filter_delete = {"id": matches['id']}
            
        #     # dao.delete(dao.dados_tipes, filter=filter_delete)
        #     # dao.delete(dao.dados_odd, filter=filter_delete)
        #     # dao.delete(dao.dados_localteam, filter=filter_delete)
        #     # dao.delete(dao.dados_visitorteam, filter=filter_delete)
        #     # dao.delete(dao.dados_ht, filter=filter_delete)
        # except:
        #     pass
        #     # dao.find_one(dao.dados_matches, filter={"ts": {"$lt": ts.t_now()}})
        log.stop()

    def gravartemp(self, dados):
        dao = MongoDao()
        obj = self.get_object(dados_json=dados)

        # Esporte
        _dict = self.c_object_to_dict(obj=obj)
        del _dict['categories']
        dao.insert(tb=dao.dados_sport, d_json=_dict)
        del _dict
        
        # Torneio
        _dict = self.c_object_to_dict(obj=obj)
        for item in _dict['categories']:
            del item['matches']
            dao.insert(tb=dao.dados_categories, d_json=item)
        del _dict

    def alterartemp(self, dados):
        dao = MongoDao()
        obj = self.get_object(dados_json=dados)

        # Esporte
        _dict = self.c_object_to_dict(obj=obj)
        del _dict['categories']
        dao.update(tb=dao.dados_sport, d_json=_dict, filter={"name": _dict["name"]})
        del _dict

        # Torneio
        _dict = self.c_object_to_dict(obj=obj)
        for item in _dict['categories']:
            del item['matches']
            dao.update(tb=dao.dados_categories, d_json=item, filter={"id": item["id"]})
        del _dict

    def select(self):
        dao = MongoDao()
        dados_dict = dao.find_one(dao.dados_sport, filter={"nome": "soccer"})
        if dados_dict:
            return dados_dict
        return None

    def get_object(self, path=None, name=None, dados_json=None):
        if not dados_json:
            if not path or not name:
                return Sport()
            with open('{0}{1}.json'.format(path, name), 'r') as fdr:
                try:
                    dados_json = json.loads(fdr.read())
                except:
                    return Sport()

        categories_json = dados_json['scores']['categories']
        lista_categories = []
        for categories_item in categories_json:
            matches_json = categories_item['matches']
            lista_matches = []
            for matches_item in matches_json:
                odds_json = matches_item['odds']
                lista_odds = []
                for odds_item in odds_json:
                    bookmakers_json = odds_item['bookmakers']
                    lista_bookmakers = []
                    for bookmakers_item in bookmakers_json:
                        lista_odd = []
                        lista_tipe = []
                        odd_json = bookmakers_item['odds']
                        for odd_item in odd_json:
                            try:
                                odd = Odd(
                                    name=odd_item['name'], value=odd_item['value'])
                                lista_odd.append(odd)
                            except:
                                for tipe_item in odd_json:
                                    tipe_odd_json = tipe_item['odds']
                                    lista_tipe_odd = []
                                    for tipe_odd_item in tipe_odd_json:
                                        odd = Odd(
                                            name=tipe_odd_item['name'], value=tipe_odd_item['value'])
                                        lista_tipe_odd.append(odd)
                                    tipe = Tipe(
                                        tipo=tipe_item['type'], name=tipe_item['name'], ismain=tipe_item['ismain'], stop=tipe_item['stop'], odd=lista_tipe_odd)
                                    lista_tipe.append(tipe)
                        bookmakers = Bookmakers(id=bookmakers_item['id'], name=bookmakers_item['name'],
                                                stop=bookmakers_item['stop'], ts=bookmakers_item['ts'], odd=lista_odd, tipe=lista_tipe)
                        lista_bookmakers.append(bookmakers)
                    odds = Odds(id=odds_item['id'], value=odds_item['value'],
                                stop=odds_item['stop'], bookmakers=lista_bookmakers)
                    lista_odds.append(odds)
                localteam = Localteam(
                    id=matches_item['localteam']['id'], name=matches_item['localteam']['name'], goals=matches_item['localteam']['goals'])
                visitorteam = Visitorteam(
                    id=matches_item['visitorteam']['id'], name=matches_item['visitorteam']['name'], goals=matches_item['visitorteam']['goals'])
                ht = Ht(score=matches_item['ht']['score'])
                matches = Matches(id=matches_item['id'], status=matches_item['status'], date=matches_item['date'], formatted_date=matches_item['formatted_date'], time=matches_item['time'], venue=matches_item['venue'],
                                  static_id=matches_item['static_id'], fix_id=matches_item['fix_id'], localteam=localteam, visitorteam=visitorteam, events=matches_item['events'], ht=ht, odds=lista_odds)
                lista_matches.append(matches)
            categories = Categories(
                name=categories_item['name'], gid=categories_item['gid'], id=categories_item['id'], matches=lista_matches)
            lista_categories.append(categories)
        sport = Sport(name=dados_json['scores']['sport'],
                      ts=dados_json['scores']['ts'], categories=lista_categories)
        return sport

    def c_dict_to_object(self, _dict):
        sport = Sport()
        if not _dict:
            return None
        sport.__dict__ = _dict
        for c_categorie in range(len(sport.categories)):
            categories = Categories()
            categories.__dict__ = sport.categories[c_categorie]
            sport.categories[c_categorie] = categories

            for c_matches in range(len(categories.matches)):
                matches = Matches()
                matches.__dict__ = categories.matches[c_matches]
                categories.matches[c_matches] = matches

                # Localteam
                localteam = Localteam()
                localteam.__dict__ = matches.localteam
                matches.localteam = localteam

                # Visitorteam
                visitorteam = Visitorteam()
                visitorteam.__dict__ = matches.visitorteam
                matches.visitorteam = visitorteam

                # Ht
                ht = Ht()
                ht.__dict__ = matches.ht
                matches.ht = ht

                # Odds
                for c_odds in range(len(matches.odds)):
                    odds = Odds()
                    odds.__dict__ = matches.odds[c_odds]
                    matches.odds[c_odds] = odds

                    # Bookmakers
                    for c_bookmakers in range(len(odds.bookmakers)):
                        bookmakers = Bookmakers()
                        bookmakers.__dict__ = odds.bookmakers[c_bookmakers]
                        odds.bookmakers[c_bookmakers] = bookmakers

                        # Odd
                        for c_odd in range(len(bookmakers.odd)):
                            odd = Odd()
                            odd.__dict__ = bookmakers.odd[c_odd]
                            bookmakers.odd[c_odd] = odd

                        # Tipe
                        for c_tipe in range(len(bookmakers.tipe)):
                            tipe = Tipe()
                            tipe.__dict__ = bookmakers.tipe[c_tipe]
                            bookmakers.tipe[c_tipe] = tipe

                            #Odd
                            for c_tipe_odd in range(len(tipe.odd)):
                                tipe_odd = Odd()
                                tipe_odd.__dict__ = tipe.odd[c_tipe_odd]
                                tipe.odd[c_tipe_odd] = tipe_odd
        return sport

    def c_object_to_dict(self, obj):
        if hasattr(obj, '__dict__'):
            obj = obj.__dict__
        if isinstance(obj, dict):
            return {k: self.c_object_to_dict(v) for k, v in obj.items()}
        elif isinstance(obj, list) or isinstance(obj, tuple):
            return [self.c_object_to_dict(e) for e in obj]
        else:
            return obj
