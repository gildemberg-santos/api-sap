# bookmakers.py
#
# Copyright 2020 Gildemberg Santos <gildemberg.santos@gmail.com>
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to
# deal in the Software without restriction, including without limitation the
# rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
# sell copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
# FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
# IN THE SOFTWARE.
#
# SPDX-License-Identifier: MIT


from src.entidade.odd import Odd
from src.entidade.tipe import Tipe


class Bookmakers(object):
    def __init__(self, id=None, name=None, stop=None, ts=None, odd=[Odd()], tipe=[Tipe()]):
        self.id = id
        self.name = name
        self.stop = stop
        self.ts = ts
        self.odd = odd
        self.tipe = tipe

    def imprimir(self):
        print('id: ', self.id)
        print('name: ', self.name)
        print('stop: ', self.stop)
        print('ts: ', self.ts)
        print('odds: ', self.odd)
        print('tipe: ', self.tipe)
