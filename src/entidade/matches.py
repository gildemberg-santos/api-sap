# matches.py
#
# Copyright 2020 Gildemberg Santos <gildemberg.santos@gmail.com>
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to
# deal in the Software without restriction, including without limitation the
# rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
# sell copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
# FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
# IN THE SOFTWARE.
#
# SPDX-License-Identifier: MIT


from src.entidade.localteam import Localteam
from src.entidade.visitorteam import Visitorteam
from src.entidade.ht import Ht
from src.entidade.odds import Odds


class Matches(object):
    def __init__(self, id=None, status=None, date=None, formatted_date=None, time=None, venue=None, static_id=None, fix_id=None, localteam=Localteam(), visitorteam=Visitorteam(), events=None, ht=Ht(), odds=[Odds()]):
        self.id = id
        self.status = status
        self.date = date
        self.formatted_date = formatted_date
        self.time = time
        self.venue = venue
        self.static_id = static_id
        self.fix_id = fix_id
        self.localteam = localteam
        self.visitorteam = visitorteam
        self.events = events
        self.ht = ht
        self.odds = odds

    def imprimir(self):
        print('id: ', self.id)
        print('status: ', self.status)
        print('date: ', self.date)
        print('formatted_date: ', self.formatted_date)
        print('time: ', self.time)
        print('venue: ', self.venue)
        print('static_id: ', self.static_id)
        print('fix_id: ', self.fix_id)
        print('localteam: ', 'id ', self.localteam.id, ' | name ',
              self.localteam.name, ' | goals', self.localteam.goals)
        print('visitorteam: ', 'id ', self.visitorteam.id, ' | name ',
              self.visitorteam.name, ' | goals', self.visitorteam.goals)
        print('events: ', self.events)
        print('ht: ', 'score ', self.ht.score)
        print('odds: ', self.odds)
