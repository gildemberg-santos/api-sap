# timestamp.py
#
# Copyright 2020 Gildemberg Santos <gildemberg.santos@gmail.com>
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to
# deal in the Software without restriction, including without limitation the
# rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
# sell copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
# FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
# IN THE SOFTWARE.
#
# SPDX-License-Identifier: MIT


from datetime import datetime
from datetime import timedelta

class Timestamp(object):
    def __init__(self):
        pass

    def t_sport(self, timestamp=None):
        if not timestamp:
            now = datetime.now()
            timestamp = int(datetime.timestamp(now))
            # timestamp -= 86400
        elif timestamp:
            timestamp = int(timestamp)
        return timestamp
    
    def t_now(self, timestamp=None):
        if not timestamp:
            now = datetime.now()
            timestamp = int(datetime.timestamp(now))
        elif timestamp:
            timestamp = int(timestamp)
        return timestamp

    def t_lost(self, timestamp=None, lost=60):
        if not timestamp:
            now = datetime.now()
            timestamp = int(datetime.timestamp(now))
        elif timestamp:
            timestamp = int(timestamp)
        return timestamp - lost 
        
    def f_ts(self, data='', time=''):
        time = "{0}:00".format(time)
        data = data.replace('.', '')
        data = "{0}-{1}-{2}".format(data[4:8],data[2:4],data[0:2])
        data = datetime.fromisoformat('{0} {1}'.format(data, time))
        ts = int(datetime.timestamp(data))
        return ts

    def f_data_str(self, ts):
        objeto_data = datetime.fromtimestamp(ts)
        data_br = objeto_data - timedelta(hours=3)
        dia = ""
        mes = ""
        if data_br.day < 10:
            dia = "0{0}".format(data_br.day)
        else:
            dia = str(data_br.day)
        if data_br.month < 10:
            mes = "0{0}".format(data_br.month)
        else:
            mes = str(data_br.month)
        texto = dia + "/" + mes + "/" + str(data_br.year)
        return texto

    def f_hora_str(self, ts):
        objeto_data = datetime.fromtimestamp(ts)
        data_br = objeto_data - timedelta(hours=3)
        hora = ""
        minuto = ""
        if data_br.hour < 10:
            hora = "0{0}".format(data_br.hour)
        else:
            hora = "{0}".format(data_br.hour)
        if data_br.minute < 10:
            minuto = "0{0}".format(data_br.minute)
        else:
            minuto = "{0}".format(data_br.minute)
        texto = hora + ":" + minuto
        return texto
