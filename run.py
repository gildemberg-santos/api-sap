#!./venv/bin/python3
# run.py
#
# Copyright 2020 Gildemberg Santos <gildemberg.santos@gmail.com>
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to
# deal in the Software without restriction, including without limitation the
# rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
# sell copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
# FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
# IN THE SOFTWARE.
#
# SPDX-License-Identifier: MIT


import os
import requests
from src.scannerjson import ScannerJSON
from src.log.logs import Logs
from src.timestamp import Timestamp
import asyncio

os.system('cls' if os.name == 'nt' else 'clear')
print('Servidor (On): Carregando...')
while True:
    ts = Timestamp()
    time_limit = 0
    log = Logs('tempo_total', output_telegram=False, output_print=True, time_limit=time_limit)
    time_limit = log.media('tempo_total')
    try:
        scn = ScannerJSON()
        loop = asyncio.get_event_loop()
        loop.run_until_complete(scn.get_odd(time_limit=int(time_limit)))
    except requests.exceptions.ConnectionError as e:
        log.gravar('Erro', e.__context__)
        print("Error: ", e.__dict__)
    except Exception as e:
        print('Erro: ', e.__context__)
        log.gravar('Erro', e.__context__)
        raise
    print("-"*80)
    log.stop()
