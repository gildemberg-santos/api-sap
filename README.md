# analise-de-apostas
Sistema para análisar oportunidades de apostas com arbitragem de casas de apostas

# Instalar Ambiente Virtual
virtualenv venv -p python3

# Ativar Ambiente Virtual
source venv/bin/activate

# Criando Requirements
venv/bin/pip3 freeze > requirements.txt

# Instalar Requirements
venv/bin/pip3 install -r requirements.txt

# Endereço do Servidor da API
https://bit.ly/2ZEQd7v
https://gist.githubusercontent.com/gildemberg-santos/97eb834beaa627d198affc244b08c0a2/raw/9ddf2f5a923615d3eb2852cb970044ec0931eeed/server-address.json

# Lista de ODDs
https://bit.ly/2ByRaX2
https://gist.githubusercontent.com/gildemberg-santos/efe2468e3ba6b00b91782cb75c717d0d/raw/b203a48cf0c5a550b0a41deae88419103eafb7d1/getodds.json

# Observações
O arquivo getodds_esports.xml tem que criar um algoritmo expecifico pra ele